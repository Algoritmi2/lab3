# Max speed, Capacity
street_types = [
    (30, 500),  # residential street
    (50, 750),  # tertiary road
    (50, 1000),  # secondary road
    (70, 1500),  # primary road
    (70, 2000),  # trunk
    (90, 4000)  # motorway
]


class RoadInfo:

    def __init__(self, capacity: int, time: float):
        self.__capacity = capacity
        self.__time = time

    @property
    def capacity(self) -> int:
        return self.__capacity

    @capacity.setter
    def capacity(self, capacity):
        self.__capacity = capacity

    @property
    def time(self) -> float:
        return self.__time

    def __float__(self):
        return self.time
