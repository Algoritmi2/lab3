import math
from typing import Tuple
from graph import Node


class MinHeap:

    def __init__(self, capacity: int = 16):
        self._nodes = [None] * capacity
        self._items = 0
        self._values_positions = {}

    def _exted_list(self, capacity: int = 16):
        self._nodes.extend([None] * capacity)

    def _left(self, i) -> int:
        return 2*i + 1

    def _right(self, i) -> int:
        return 2*i + 2

    def _parent(self, i) -> int:
        return int(math.floor((i - 1) / 2))

    def _bubble_up(self, i):
        p = self._parent(i)
        while i > 0 and self._nodes[i][0] < self._nodes[p][0]:
            self._nodes[p], self._nodes[i] = self._nodes[i], self._nodes[p]
            # Updates values position
            self._values_positions[self._nodes[i][1].id] = i
            self._values_positions[self._nodes[p][1].id] = p
            i = p
            p = self._parent(i)

    def _trickle_down(self, i):
        l = self._left(i)
        r = self._right(i)
        n = self._items
        smallest = i
        if l < n and self._nodes[l][0] < self._nodes[i][0]:
            smallest = l

        if r < n and self._nodes[r][0] < self._nodes[smallest][0]:
            smallest = r

        if smallest != i:
            self._nodes[i], self._nodes[smallest] = self._nodes[smallest], self._nodes[i]
            # Updates values position
            self._values_positions[self._nodes[i][1].id] = i
            self._values_positions[self._nodes[smallest][1].id] = smallest
            self._trickle_down(smallest)

    def add(self, key: float, value: Node):
        n = self._items
        if n >= len(self._nodes):
            self._exted_list()
        self._values_positions[value.id] = n
        self._nodes[n] = (key, value)
        self._items += 1
        self._bubble_up(n)

    def decrease_key(self, value: Node, new_key: float) -> bool:
        i = self._values_positions[value.id]
        key, value = self._nodes[i]
        if key < new_key:
            return False
        self._nodes[i] = (new_key, value)
        self._values_positions[value.id] = i
        self._bubble_up(i)
        return True

    def extract_min(self) -> Tuple[float, Node]:
        min_val = self._nodes[0]
        self._items -= 1
        if self._items > 0:
            self._nodes[0] = self._nodes[self._items]
            self._nodes[self._items] = None
            self._values_positions[self._nodes[0][1].id] = 0
            self._trickle_down(0)
        else:
            self._nodes[0] = None
            self._values_positions.clear()
        return min_val

    def __len__(self):
        return self._items

