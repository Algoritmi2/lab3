Domanda 2

Il massimo numero di veicoli che possono entrare in città dalle sorgenti è 20000, il numero di veicoli che possono arrivare
contemporaneamente alle destinazioni sono 12000, mentre la capacità massima del piano trovato è 10500 veicoli. Il collo di bottiglia
si trova nel nodo 65286004 che ha il numero massimo di archi entranti (14) e quindi sarà il punto più congestionato.

Domanda 3

La coda di priorità nell'algoritmo di Dijkstra è stata implementata mediante un Min Heap che mantiene ordinati i nodi del grafo in
maniera decrescente rispetto al tempo di percorrenza.
Tempi di complessità:
- creazione della coda O(n log n)
- _trickle_down O(log n)
- extractMin O(log n)
- _bubble_up O(log n)
- decreaseKey O(log n)