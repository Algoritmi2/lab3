import math
from typing import Set, Dict, List

from graph import Graph, Node
from heap import MinHeap
from model import RoadInfo
from performance import execution_time


def init_ssp(graph: Graph, source: Node) -> tuple:
    d = {}
    pi = {}
    for node_id, node in graph:
        d[node_id] = math.inf
        pi[node_id] = None
    d[source.id] = 0
    return d, pi


def relax(graph: Graph, tail: Node, head: Node, d: Dict, pi: Dict):
    weight_value = graph.weight(tail, head).__float__()
    d[head.id] = d[tail.id] + weight_value
    pi[head.id] = tail.id
    return d, pi


def dijkstra(graph: Graph, source: Node):
    d, pi = init_ssp(graph, source)

    q: MinHeap = MinHeap()
    for v_id, v in graph:
        q.add(d[v_id], v)

    while len(q) != 0:
        key, u = q.extract_min()
        for v in graph.neighbors(u):
            weight = graph.weight(u, v)  # type: RoadInfo
            weight_value = weight.time
            d_u = d[u.id]
            d_v = d[v.id]
            if d_u + weight_value < d_v:
                d, pi = relax(graph, u, v, d, pi)
                q.decrease_key(v, d[v.id])
    return d, pi


def get_path(graph: Graph, super_source: Node, destinations: Set[Node]):
    d, pi = dijkstra(graph, super_source)

    min_value = math.inf
    target_dest = None
    for dest in destinations:
        if min_value > d[dest.id]:
            min_value = d[dest.id]
            target_dest = dest

    path = []
    if target_dest is not None:
        v = target_dest.id
        while v is not None:
            path.insert(0, v)
            v = pi.get(v, None)

    return path


@execution_time
def ccrp(graph: Graph, sources: Set[Node], destinations: Set[Node]):
    super_source = Node('-1', '-1')
    graph.add(super_source)
    for source in sources:
        graph.connect(super_source, source, RoadInfo(math.inf, 0))

    plan = []
    capacities = [0]
    times = [0]
    while True:
        path = get_path(graph, super_source, destinations)
        if len(path) == 0:
            break

        plan.append(path)
        flow = math.inf
        time = 0

        for i in range(0, len(path) - 1):
            road_info = graph.weight(graph[path[i]], graph[path[i + 1]])  # type: RoadInfo
            time += road_info.time
            if flow > road_info.capacity:
                flow = road_info.capacity

        for i in range(0, len(path) - 1):
            tail = graph[path[i]]
            head = graph[path[i + 1]]
            road_info = graph.weight(tail, head)  # type: RoadInfo
            new_capacity = road_info.capacity - flow
            if new_capacity == 0:
                graph.disconnect(tail, head)
            else:
                graph.update_weight(tail, head, RoadInfo(new_capacity, road_info.time))

        capacity = flow
        if len(capacities) > 0:
            capacity += capacities[-1]
        capacities.append(capacity)

        new_time = time
        if len(times) > 0:
            if times[-1] > new_time:
                new_time = times[-1]
        times.append(new_time)

    return plan, times, capacities
