from typing import List, Dict, Generic, TypeVar


class Node:

    def __init__(self, id, value = None):
        self._id = id
        self._value = value

    def _get_id(self):
        return self._id

    def _get_value(self):
        return self._value

    id = property(_get_id)
    value = property(_get_value)


T = TypeVar("T")


class Graph(Generic[T]):

    def __init__(self):
        self.__nodes = {}  # type: Dict[str, Node]
        self.__weights = {}  # type: Dict[str, Dict[Node, T]]

    def add(self, node: Node):
        self.__nodes[node.id] = node
        self.__weights[node.id] = {}

    def remove(self, node: Node):
        self.__nodes.pop(node.id)
        self.__weights.pop(node.id)

    def connect(self, tail: Node, head: Node, weight: T = None, both=False):
        self.__weights[tail.id][head.id] = weight
        if both:
            self.__weights[head.id][tail.id] = weight

    def disconnect(self, tail: Node, head: Node):
        self.__weights[tail.id].pop(head.id)

    def weight(self, tail: Node, head: Node) -> T:
        return self.__weights[tail.id][head.id]

    def update_weight(self, tail: Node, head: Node, weight: T):
        self.__weights[tail.id][head.id] = weight

    def neighbors(self, v: Node) -> List[Node]:
        keys = self.__weights[v.id].keys()
        nodes = list(map(lambda n_id: self.__nodes[n_id], keys))
        return nodes

    def are_connected(self, tail: Node, head: Node) -> bool:
        return head.id in self.__weights[tail.id]

    def contains(self, node: Node):
        return node.id in self.__nodes

    def __contains__(self, item) -> bool:
        if item is str:
            return item in self.__nodes
        if item is Node:
            return item.id in self.__nodes
        return False

    def __getitem__(self, node_id: str) -> Node:
        return self.__nodes[node_id]

    def __iter__(self):
        return map(
            lambda data:
            (data[0], data[1]),
            self.__nodes.items())

