from typing import List

from graph import Graph, Node
from algorithms import ccrp
from model import RoadInfo
import matplotlib.pyplot as plt
import numpy as np


def parse_graph(file_path: str) -> Graph[RoadInfo]:
    result_graph: Graph[RoadInfo] = Graph()
    street_types = [
        (30, 500),  # residential street
        (50, 750),  # tertiary road
        (50, 1000),  # secondary road
        (70, 1500),  # primary road
        (70, 2000),  # trunk
        (90, 4000)]  # motorway

    with open(file_path, 'r') as file:
        for line in file:
            values = line.replace('\n', '').split('\t')
            if len(values) == 4:
                source = Node(values[0])
                destination = Node(values[1])
                length = float(values[2]) / 1000
                speed, capacity = street_types[int(values[3]) - 1]
                road_info = RoadInfo(capacity, length / speed * 3600)  # tempo di percorrenza, capacità della strada
                if not result_graph.contains(source):
                    result_graph.add(source)
                if not result_graph.contains(destination):
                    result_graph.add(destination)
                if not result_graph.are_connected(source, destination):
                    result_graph.connect(source, destination, road_info)
    return result_graph


def print_capacity_time_graph(capacity, times):
    plt.subplot(2, 1, 1)  # number_of_rows, num_of column, position, first position
    plt.plot(range(len(capacity)), capacity, label="Capacità")
    plt.xlabel("Numero di cammini")
    plt.ylabel("Capacita'")
    plt.legend()
    plt.title("CCRP - Capacità massima", y=1.08)
    plt.xticks(np.arange(0, len(capacity), 1))
    plt.grid()

    # p creiamo il secondo subplot (in verticale)
    plt.subplot(2, 1, 2)  # seconda posizione
    plt.plot(range(len(times)), times, label="Tempo (s)", color='red')
    plt.xlabel("Numero di cammini")
    plt.ylabel("Tempo (s)")
    plt.title("CCRP - Tempo impiegato", y=1.08)
    plt.legend()
    plt.xticks(np.arange(0, len(times), 1))
    plt.yticks(np.arange(0, 800, 100))
    plt.grid()

    plt.subplots_adjust(hspace=0.65)
    plt.savefig("grafico-affiancato.png")
    plt.show()


def find_bottle_neck(plan: List[List[str]]):
    in_deg = {}
    for path in plan:
        for i in range(1, len(path)):
            in_deg[path[i]] = 1 + in_deg.get(path[i], 0)
    sorted_list = sorted(in_deg.items(), key=lambda t: t[1], reverse=True)
    print("Il collo di bottiglia si trova nel nodo: {0} che ha {1} archi entranti".format(sorted_list[0][0], sorted_list[0][1]))


def main():
    graph: Graph[RoadInfo] = parse_graph('dataset.txt')
    sources = set([graph['3718987342'], graph['915248218'], graph['65286004']])
    destinations = set([graph['261510687'], graph['3522821903'], graph['65319958'],
                        graph['65325408'], graph['65295403'], graph['258913493']])

    total_source = 0
    for s in sources:
        total_source += sum(map(lambda n: graph.weight(s, n).capacity, graph.neighbors(s)))

    total_dest = 0
    for id, node in graph:
        if node in destinations:
            continue

        neighbors = graph.neighbors(node)
        for n in neighbors:
            if n in destinations:
                total_dest += graph.weight(node, n).capacity

    print("Capacità massima delle sorgenti ", total_source)
    print("Capacità massima delle destinazioni", total_dest)

    plan, times, capacity = ccrp(graph, sources, destinations)

    print("Capacità del piano ", capacity[-1])

    find_bottle_neck(plan)

    print_capacity_time_graph(capacity, times)


if __name__ == '__main__':
    main()
